# JupyterHub on PLMshift

Pierre Navaro - Scientific computing engineer
[CNRS-IRMAR](https://irmar.univ-rennes.fr)-[Groupe Calcul](https://calcul.math.cnrs.fr)

Bordeaux, 19-23 june 2023

https://navarop.gitlabpages.inria.fr/inriameetings/plmshift

## Warning

*This material will serve as a guideline for my talk but does not constitute documentation. To build your application, you should use the most recent version of the documentation edited by the PLM team.*

https://plmshift.pages.math.cnrs.fr/

---

# Why me ?

- Nice recommendation from Mathrice people.
- One of the first external user of the platform.
- Need to run a beta version of the python package [fdasrsf](https://fdasrsf-python.readthedocs.io/en/latest/).
- I needed to make a demo for an industrial partner with limited access to digital resources.
- [GitHub+Binder](https://binderhub.readthedocs.io/en/latest/) solution was not possible because the data had to remain confidential.
- Exposing web services outside the network of the University of Rennes is very complicated or even impossible...


---

# PLMshift

- Allows to provision, instantiate, run, and manage web applications, without the complexity of building and maintaining the infrastructure.
- No need to configure and administer an OS.
- Define your resources and the system makes them available.
- An application is a web-based service that uses CPU and RAM: Website (static or CMS), R shiny application, Jupyter notebooks...

## Glossary

- **Project**: used to group and isolate related applications and manage user access.
- **Applications**: service created from templates, files, images, ...
- **Build configuration**: parameters of your application.
- **Builds**: process of transforming input parameters into a resulting object.
- **Deployment**: management of the life cycle of an application.
- **Pods**: provide the runtime environments 
- **Routes**: exposes a service at a host name.

---

# How to deploy your application
- Configure your application from a catalog with a graphical interface on the web.
- With a description file (yaml or json format)
- With Helm3 (did not try)

[OKD docs: Building applications overview](https://docs.okd.io/4.9/applications/index.html)

[PLM docs: Déploiement d’applications sur PLMshift](https://indico.math.cnrs.fr/event/4309/contributions/3521/attachments/2272/2750/PLMshift_pour_le_chercheur.pdf) 

---

## Deploy from the catalog

- Connect to https://plmshift.math.cnrs.fr
- Select the `Developer` mode
- Create a project with `+Add` 
- Create an application with `Add to Project`
- Use `Developer Catalog` to find your application
- Left Menu -> Project -> Overview -> Route
- Go to <https://app-project.apps.math.cnrs.fr>

---

## Deploy with openshift CLI

- Download from https://plmshift.math.cnrs.fr/command-line-tools or on MacOS
```bash
brew install openshift-cli
```
- Get your token by clicking on your name (top-right) and select `Copy login command`
```bash
oc login --token=xxx --server=https://api.math.cnrs.fr:6443
```

---

## Simple Notebook

Create new project and witch to this project
```bash
oc new-project myproject 
oc project myproject 
```
Display parameters
```bash
oc process --parameters notebook-deployer -n openshift
```
Instantiate your notebook
```bash
oc new-app notebook-deployer -p APPLICATION_NAME=<NotebookName> -p NOTEBOOK_PASSWORD=<StrongPassword>
```
Get the deployment name
```bash
oc get dc
```
Display logs
```bash
oc logs -f dc/<NotebookName>
```

---

# Deploy a JupyterHub server

## Deploy JupyterHub from catalog

- Select or create a project
- Create an app using **JupyterHub on PLMshift**
- Follow instructions

## Deploy JupyterHub with CLI

```bash
oc process --parameters  jupyterhub-odh -n openshift
oc new-app jupyterhub-odh -p APPLICATION_NAME=monjupyterhub -p JUPYTERHUB_HOST=monjupyterhub.apps.math.cnrs.fr
```

Change configuration
```bash
oc edit kfdef
```

---

## Enable GPU
```yaml
    - kustomizeConfig:
        overlays:
          - storage-class
          - plm
        parameters:
          ...
          - name: gpu_enabled
            value: 'true'
        repoRef:
          name: manifests
          path: jupyterhub/jupyterhub
```
## Make GPU kernels available
```yaml
    - kustomizeConfig:
        overlays:
          ...
          - cuda-plmshift
        repoRef:
          name: manifests
          path: jupyterhub/notebook-images
      name: notebook-images
```

---

## Custom kernel 

- Fork https://plmlab.math.cnrs.fr/plmshift/notebook-example
- Change the `requirements.txt` file to add your dependencies
- Example: add a kernel with [dask](https://docs.dask.org/en/stable/) 
```
dask
graphviz
pandas
```
- `Catalog -> Notebook Builder on PLMshift` 
    + Set name: `dask`
    + Set repository url ending with `.git`
    + Set description: `Dask kernel`
    + Set project, leave empty to link with your current project
    + `dask` should appear on your jupyterhub home page

---

# Custom kernel with command line

```bash
oc new-app --list # get list of kernels
```

display parameters
```bash
oc process --parameters notebook-builder-odh -n openshift
```

Create kernel
```bash
oc new-app notebook-builder-odh \
-p NOTEBOOK_NAME=dask-kernel \
-p NOTEBOOK_REPO_URL=https://plmlab.math.cnrs.fr/navaro/notebook-example.git \
-p NOTEBOOK_DESC="kernel with dask"
```

---

# Rebuild your kernel

- Change the requirements and push to the git repository
```bash
oc get bc
oc start-build bc/<BuildConfigName>
oc logs -f bc/<BuildConfigName>
```
- Change parameters
```bash
oc edit bc/<BuildConfigName>
oc start-build bc/<BuildConfigName>
oc logs -f bc/<BuildConfigName>
```

---

# Configuration change triggers

- Create a webhook
```bash
oc set triggers bc/<BuildConfigName> --from-gitlab
```
- Copy the webhook url with secret in console `Builds -> Build Configs -> Build Config Details`
- Create the webhook on gitlab in `Settings -> Webhooks -> URL`
- Enable `Trigger -> Push events` and `Test` !
```bash
oc get bc
oc describe bc/<BuildConfigName>
oc logs -f build/<BuildName>
```

---

# Private repository

- Get an Access Token on PLMlab: `Settings -> Access Tokens`
- Give a value to the `Token name`, check `read_repository` and click on `Create project access token`.
- Save values of `Token name` and `Secret` because you will not be able to access it afterwards.

Create the secret
```bash
oc create secret generic <SecretName> \
    --from-literal=username=<TokenName> \
    --from-literal=password=<Token> \
    --type=kubernetes.io/basic-auth
```

Build your application
```bash
oc get bc 
oc set build-secret --source bc/<BuildConfig> <SecretName>
oc start-build bc/<BuildConfig>
```

---

# PLMshift web console

- Go to the PLMShift console, select your project
`Secrets -> Create -> Source Secret tab`
- For`Authentication Type` select`Basic Authentication`
- Enter the `<UserName>` and `<TokenName>` in the corresponding fields
- Click on the `Create` button
- Then add the secret in the Build Config (the command via `oc` is recommended for this action)

---

# Useful commands to manage your application

- List the projects 
```bash
oc projects
```
- Switch to another project
```bash
oc project <projectname>
```
- Remove an app 
```bash
oc delete all --selector app=myapp
```
- Re-deploy an app 
```bash
oc get dc
oc rollout latest dc/<name>
```

---
- Display log 
```bash
oc logs -f dc/<name>
```
- Routes to your applications 
```bash
oc get route
```
- Build configurations 
```bash
oc get bc
```
- Running pods 
```bash
oc get pods
```
- Log to the pod 
```bash
oc rsh <podname>
```
- Copy files on the pod 
```bash
oc rsync mypod:dir/file
oc cp mypod:dir/file
```

---

# Performing and configuring builds

```bash
oc status # do it as much as you want
oc get bc  # get list
oc start-build <BuildConfigName> --follow # start and display logs
oc cancel-build <BuildName> # cancel one build
oc cancel-build bc/<BuildConfigName> # cancel all builds from a config
oc delete bc <BuildConfigName> # delete build config
oc describe build <BuildName> # build details
oc logs -f bc/<BuildConfigName> # accessing logs
oc edit bc/<BuildConfigName> # edit config
```

---

# Deploy R shiny app

- fork https://plmlab.math.cnrs.fr/plmshift/shiny-custom
- In `ShinyApps` directory, change `ui.R` and `server.R`
- Update `requirements.txt` with R packages dependencies list.
- Create an plmshift application with `Shiny R Application Server` template
- Don't forget to end with `.git` when you set the url of your repository.

---

# PLM-team references

- [PLMshift: Documentation](https://plmshift.pages.math.cnrs.fr)
- [PLMshift: Deploy JupyterHub](https://plmshift.pages.math.cnrs.fr/exemples_de_deploiement/notebook_jupyter/jupyterhub/)
- [PLMshift: Deploy a simple notebook](https://plmshift.pages.math.cnrs.fr/exemples_de_deploiement/notebook_jupyter/notebook/)
- [PLMshift: Deploy a notebook with dependencies](https://plmshift.pages.math.cnrs.fr/exemples_de_deploiement/notebook_jupyter/notebook_custom/)
- [PLMshift: Use PLMShift with a private repository](https://plmshift.pages.math.cnrs.fr/Foire_aux_Questions/acces_depot_prive/)
- [Talk: Présentation de l'offre PaaS PLMshift dont les Notebooks Jupyter](https://indico.math.cnrs.fr/event/6772/contributions/5842/attachments/3109/4160/plmshift_et_gpu.pdf)
- [Talk: Anatomie d'une application openshift](https://indico.math.cnrs.fr/event/4309/contributions/3521/attachments/2272/2750/PLMshift_pour_le_chercheur.pdf)

https://mattermost.math.cnrs.fr/plm-support/channels/plmshift

---

# OKD Docs

- [docs.okd.io : Applications](https://docs.okd.io/latest/applications/index.html)
- [docs.okd.io : Source clone secrets](https://docs.okd.io/latest/cicd/builds/creating-build-inputs.html#builds-adding-source-clone-secrets_creating-build-inputs)
- [docs.okd.io : Triggering builds](https://docs.okd.io/latest/cicd/builds/triggering-builds-build-hooks.html#builds-configuration-change-triggers_triggering-builds-build-hooks)

---

Questions ?
