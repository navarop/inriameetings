# Inria Meetings

Supports pour la rencontre thématique HPC et les [journées devlog INRIA](https://devlog.gitlabpages.inria.fr/2023-seminaire/programme/) à Bordeaux du 19 au 23 juin 2022 à Bordeaux.

- [Supports Julia for HPC](https://navarop.gitlabpages.inria.fr/inriameetings/)
- [PLMshift la plateforme openshift des mathématiciens du CNRS](https://navarop.gitlabpages.inria.fr/inriameetings/plmshift)
- [quarto](https://navarop.gitlabpages.inria.fr/inriameetings/quarto)

## Run HPC examples

```bash
cd JuliaHPC
julia --project
julia> import Pkg; Pkg.instantiate()
julia> include("examples/04-grayscott.jl")
```


## Configuration

- [CUDA.jl](https://cuda.juliagpu.org/dev/installation/overview/)
- [MPI.jl](https://juliaparallel.org/MPI.jl/stable/configuration/)
